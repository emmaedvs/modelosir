from tkinter import *
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

root = Tk()

root.geometry("400x400")
root.title("Modelo SIR")
root.iconbitmap("C:/Users/emmam/PycharmProjects/sirmodel/iconos/icono.ico")
root.resizable(False, False)

# Emmanuel David Ventura Silva
# Modelo SIR
# Simulación

# Población total, N.
N = 2942

# Número inicial de individuos infectados y recuperados, I0 y R0.
I0, R0 = 1, 0

# Cualquier otro, S0, es susceptible de infectarse al inicio
S0 = N - I0 - R0

# Tasa de infección, beta, y tasa de recuperación, gama. (en 1/días)
dias = 203
beta, gamma = 0.2, 1. / dias

# Un arreglo de puntos en el tiempo (en días)
t = np.linspace(0, 250, 250)


# Las ecuaciones diferenciales del modelo SIR.
def deriv(y, t, N, beta, gamma):
    S, I, R = y
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    return dSdt, dIdt, dRdt


# Vector de condiciones iniciales
y0 = S0, I0, R0
# Integración de las ecuaciones SIR sobre el vector de tiempo, t.
ret = odeint(deriv, y0, t, args=(N, beta, gamma))
S, I, R = ret.T

# Traza de los datos en tres curvas separadas para S(t), I(t) y R(t)
def inicio():
    fig = plt.figure(facecolor='w')
    ax = fig.add_subplot(111)
    ax.plot(t, S / 1000, 'b', alpha=0.5, lw=2, label='Suceptible')
    ax.plot(t, I / 1000, 'r', alpha=0.5, lw=2, label='Infectado')
    ax.plot(t, R / 1000, 'g', alpha=0.5, lw=2, label='Recuperado')
    ax.set_xlabel('Tiempo (' + str(dias) + ' días)')
    ax.set_ylabel('Tasa (' + str(N) + ' casos)')
    ax.set_ylim(0, 3.0)
    ax.yaxis.set_tick_params(length=0)
    ax.xaxis.set_tick_params(length=0)
    ax.grid(b=True, which='major', c='w', lw=2, ls='-')
    legend = ax.legend()
    legend.get_frame().set_alpha(0.5)
    plt.title("Gráfica")
    for spine in ('top', 'right', 'bottom', 'left'):
        ax.spines[spine].set_visible(False)

    plt.show()

boton = Button(root, text="Lanzar gráfica", command=inicio)
boton.pack()

root.mainloop()
