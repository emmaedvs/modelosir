# Importación de librerías
from tkinter import *
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Emmanuel David Ventura Silva
# Modelo SIR
# Simulación

# Declaración de la ventana principal
root = Tk()

# Tamaño de la ventana
ancho = 400
largo = 400

anchop = root.winfo_screenwidth()
largop = root.winfo_screenheight()

# Inicio de la ventana en la pantalla
x = (anchop / 2) - (ancho / 2)
y = (largop / 2) - (largo / 2)

# Propiedades de la ventana
root.geometry("%dx%d+%d+%d" % (ancho, largo, x, y))
root.title("Modelo SIR - INFLUENZA 2019 -2020")
root.iconbitmap("C:/Users/emmam/PycharmProjects/sirmodel/iconos/icono.ico")
root.resizable(False, False)
root['bg'] = '#FF9933'


# Función principal
def exect(no, di, tit):
    # Población total, N.
    N = no

    # Número inicial de individuos infectados y recuperados, I0 y R0.
    I0, R0 = 1, 0

    # Cualquier otro, S0, es susceptible de infectarse al inicio
    S0 = N - I0 - R0

    # Tasa de infección, beta, y tasa de recuperación, gama. (en 1/días)
    dias = di
    beta, gamma = 0.2, 1. / dias

    # Un arreglo de puntos en el tiempo (en días)
    t = np.linspace(0, 250, 250)

    # Vector de condiciones iniciales
    y0 = S0, I0, R0
    # Integración de las ecuaciones SIR sobre el vector de tiempo, t.
    ret = odeint(deriv, y0, t, args=(N, beta, gamma))
    S, I, R = ret.T

    inicio(N, dias, S, I, R, t, tit)


# Las ecuaciones diferenciales del modelo SIR.
def deriv(y, t, N, beta, gamma):
    S, I, R = y
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    return dSdt, dIdt, dRdt


# Traza de los datos en tres curvas separadas para S(t), I(t) y R(t)
def inicio(no, di, s, i, r, t, tit):
    fig = plt.figure(facecolor='w')
    ax = fig.add_subplot(111)
    ax.plot(t, s / 1000, 'b', alpha=0.5, lw=2, label='Suceptible')
    ax.plot(t, i / 1000, 'r', alpha=0.5, lw=2, label='Infectado')
    ax.plot(t, r / 1000, 'g', alpha=0.5, lw=2, label='Recuperado')
    ax.set_xlabel('Tiempo (' + str(di) + ' días)')
    ax.set_ylabel('Tasa (' + str(no) + ' casos)')
    ax.set_ylim(0, 3.0)
    ax.yaxis.set_tick_params(length=0)
    ax.xaxis.set_tick_params(length=0)
    ax.grid(b=True, which='major', c='w', lw=2, ls='-')
    legend = ax.legend()
    legend.get_frame().set_alpha(0.5)
    plt.title("Gráfica " + tit)
    for spine in ('top', 'right', 'bottom', 'left'):
        ax.spines[spine].set_visible(False)

    plt.show()


# Función para el botón otra
def des():
    btn5['state'] = DISABLED
    e1['state'] = NORMAL
    e2['state'] = NORMAL
    e3['state'] = NORMAL
    btn6['state'] = NORMAL
    e1.delete(0, END)
    e2.delete(0, END)
    e3.delete(0, END)


# Función que sirve el botón aceptar
def act(a, b, c):
    btn5['state'] = NORMAL
    e1['state'] = DISABLED
    e2['state'] = DISABLED
    e3['state'] = DISABLED
    btn6['state'] = DISABLED
    exect(a, b, c)


# Creación de los botones con sus atributos y acciones
btn1 = Button(root, text="A(H3N2)", width=10, command=lambda: exect(1213, 203, "influenza A(H3N2)"))
btn2 = Button(root, text="B", width=10, command=lambda: exect(1907, 203, "influenza B"))
btn3 = Button(root, text="A(H1N1)", width=10, command=lambda: exect(2942, 203, "influenza A(H1N1)"))
btn4 = Button(root, text="A", width=10, command=lambda: exect(190, 203, "influenza A"))
btn5 = Button(root, text="Otra", width=24, command=des)
btn6 = Button(root, text="Aceptar", width=10, state=DISABLED,
              command=lambda: act(int(e1.get()), int(e2.get()), e3.get()))

# Creación de las entradas y etiquetas con sus atributos
e1 = Entry(root, width=10, state=DISABLED)
e2 = Entry(root, width=10, state=DISABLED)
e3 = Entry(root, width=10, state=DISABLED)
l1 = Label(root, text="\nSeleccione la influenza que desea visualizar", bg="#FF9933")
l2 = Label(root, text="\nIngrese los datos", bg="#FF9933")
l3 = Label(root, text="Población", bg="#FF9933")
l4 = Label(root, text="Días", bg="#FF9933")
l5 = Label(root, text="Enfermedad", bg="#FF9933")

# Se agregan los botones,
# entradas y etiquetas a la ventana
l1.grid(row=0, column=0, columnspan=4)
btn1.grid(row=1, column=0)
btn2.grid(row=1, column=1)
btn3.grid(row=2, column=0)
btn4.grid(row=2, column=1)
btn5.grid(row=3, column=0, columnspan=2)
l2.grid(row=4, column=0, columnspan=4)
l3.grid(row=5, column=0)
l4.grid(row=6, column=0)
l5.grid(row=7, column=0)
e1.grid(row=5, column=1)
e2.grid(row=6, column=1)
e3.grid(row=7, column=1)
btn6.grid(row=8, column=0, columnspan=2)

# Inicio del hilo de la ventana principal
root.mainloop()
